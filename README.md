# README #

This README is a set up guide for the BankAccount application.

## Quick summary ##

Bank Account Application is a simple micro web service developed to mimic a �Bank Account� operations. It has been develloped using Java, Spring MVC, SQLite and Apache Tomcat Server.
## Version: 
1.0
## Environment ##
This application has been developed under the following environment:

1. Ubuntu 16.04
2. Java (JDK 1.8)
3. Apache Tomcat 7
4. Gradle 4.4.1
5. SQLite 3
6. Git 2.7.4

## Set up ##
We assume that Java, Gradle and Tomcat are already well installed and configured.

* Check out the code from the repository by running the following command: 

```
#!script

git clone https://sasipa90@bitbucket.org/sasipa90/bankaccount.git
```


* From the command line run the following command to build the project 
```
#!script

gradle war
```

* Deploy the application to your local tomcat instance by running the following script from the command line


```
#!script

gradel tomcatRun
```

You should be able to see a successful message from the screen that looks like  

![picture](/img/DeploymentSuccess.png)


## API Services and parameters ##

**1. Balance**

URL: http://{server_url}/{application}/balance/{account number}
Method: GET

sample: http://localhost:8080/BankAccount/balance/ACC0001

**ACC001** is set as default account

**2. Deposit**

URL: http://{server_url}/{application}/deposit

Method : POST

Parameters: 

* amount (required)
* accountNo (not required, set *ACC001* as default)

Sample Request: http://localhost:8080/BankAccount/deposit

Sample Response: "success":true,"statusCode":200,"message":"Successfully deposited $10000.0 to account No ACC001. Your new balance is $54400.0."}

**3. Withdraw** 

URL: http://{server_url}/{application}/withdraw

Method : POST

Parameters: 

* amount (required)
* accountNo (not required, set *ACC001* as default)

Sample Request: http://localhost:8080/BankAccount/withdraw

Sample Response: {"success":false,"statusCode":200,"message":"Exceeded Maximum Withdrawal Frequency"}

## Dependencies ##
1. Spring Web MVC
2. Spring JDBC
3. Log4j
4. Jackson JSON
5. SQLite JDBC
6. Commons Database Connection Pools


## Configuration ##

The applications has two main configurations:

1. bankaccount.properties: Has application rules and messages configurations.
2. log4j.properties : Has logging related configurations. Make sure that the configured logging directory exists. The default path is **/var/log/bank_account**

The configurations files can be updated but require the server to be restarted to take effect.

They can be found in tomcat ***/webapps/BankAccount/WEB-INF/classes/*** directory.

## Run and Generating Test Results ##

Run the test with the command 
```
#!shell

gradle clean test
```
 from the project root folder.

After the tests, the results will be shown as in the image below 

![picture](/img/TestResults.png)

Below are the test results:

![picture](/img/TestSummary.png)

## Code Coverage Report ##

To generate the code coverage report, run from the project root folder  
```
#!shell

gradle clean test jacocoTestReport
```
 or run 
```
```

You can then access it through a browser in the target folder.

Sample path: file:///home/xxxx/projects/github/bankaccount/build/reports/jacoco/test/html/index.html

![picture](/img/JavaCodeCoverageResults.png)

## Author ##

* Parfait Pascal Sibomana <sasipa90@hotmail.com>
