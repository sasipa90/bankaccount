package com.tala.sasipa.dao;

import com.tala.sasipa.dao.impl.BankAccountDAOImpl;
import com.tala.sasipa.model.BankAccount;
import com.tala.sasipa.utils.Props;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.sql.SQLException;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:springrest-servlet.xml"})
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BankAccountDAOTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private static final String accountNo = UUID.randomUUID().toString();

    @Before
    public void setUp() {

        BankAccountDAOImpl bankAccountDAOImpl = (BankAccountDAOImpl) webApplicationContext
                .getBean("bankAccountDAO");
        Props prop = (Props) webApplicationContext.getBean("props");
        bankAccountDAOImpl.setupAccount(accountNo, prop);

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    public void testSQLException() throws Exception {

        BankAccountDAOImpl bankAccountDAOMock = Mockito.mock(BankAccountDAOImpl.class);
        Props propsMock = Mockito.mock(Props.class);
        bankAccountDAOMock.setJdbcTemplate(null);
        BankAccount bAcc = bankAccountDAOMock.getAccountInformations(accountNo, propsMock);
        assertEquals(null, bAcc);
    }


    @Test
    public void getAccountInformations() {
        Props p = (Props) webApplicationContext.getBean("props");
        JdbcTemplate jdbcTemplate = (JdbcTemplate) webApplicationContext
                .getBean("jdbcTemplate");
        BankAccountDAOImpl bankAccountDAOImpl = (BankAccountDAOImpl) webApplicationContext
                .getBean("bankAccountDAO");
        BankAccount account = bankAccountDAOImpl.getAccountInformations(accountNo, p);
        assertNotNull("Bank Account Is NULL", account);


        BankAccount newAccount = bankAccountDAOImpl.getAccountInformations(
                UUID.randomUUID().toString(), p);
        assertNull("Bank Account Is NOT NULL", newAccount);

        BankAccount nullAccount = bankAccountDAOImpl.getAccountInformations(
                null, p);
        assertNull("Bank Account Is NOT NULL", newAccount);


    }

    @Test
    public void deposit() {

        Props p = (Props) webApplicationContext.getBean("props");
        JdbcTemplate jdbcTemplate = (JdbcTemplate) webApplicationContext
                .getBean("jdbcTemplate");
        BankAccountDAOImpl bankAccountDAOImpl = (BankAccountDAOImpl) webApplicationContext
                .getBean("bankAccountDAO");
        String accountNo = UUID.randomUUID().toString();
        bankAccountDAOImpl.setupAccount(accountNo, p);
        BankAccount account = bankAccountDAOImpl.getAccountInformations(accountNo, p);
        bankAccountDAOImpl.deposit(account, 1000, p);
        account = bankAccountDAOImpl.getAccountInformations(accountNo, p);
        assertEquals("Bank Account Balance is Incorrect",1700.0,account.getBalance(), 0.1);

    }

    @Test
    public void withdraw() {

        Props p = (Props) webApplicationContext.getBean("props");
        JdbcTemplate jdbcTemplate = (JdbcTemplate) webApplicationContext
                .getBean("jdbcTemplate");
        BankAccountDAOImpl bankAccountDAOImpl = (BankAccountDAOImpl) webApplicationContext
                .getBean("bankAccountDAO");
        String accountNo = UUID.randomUUID().toString();
        bankAccountDAOImpl.setupAccount(accountNo, p);
        BankAccount account = bankAccountDAOImpl.getAccountInformations(accountNo, p);
        bankAccountDAOImpl.withdraw(account, 500, p);
        account = bankAccountDAOImpl.getAccountInformations(accountNo, p);
        assertEquals("Bank Account Balance is Incorrect",200.0,account.getBalance(), 0.1);

    }

    @Test
    public void setupAccount() {
        Props p = (Props) webApplicationContext.getBean("props");
        JdbcTemplate jdbcTemplate = (JdbcTemplate) webApplicationContext
                .getBean("jdbcTemplate");
        BankAccountDAOImpl bankAccountDAOImpl = (BankAccountDAOImpl) webApplicationContext
                .getBean("bankAccountDAO");
        BankAccount account = bankAccountDAOImpl.getAccountInformations(accountNo, p);
        assertEquals("Bank Account Balance is Incorrect", 700.0, account.getBalance(), 0.1);
    }

    @Test
    public void getJdbcTemplate() {
        Props p = (Props) webApplicationContext.getBean("props");
        JdbcTemplate jdbcTemplate = (JdbcTemplate) webApplicationContext
                .getBean("jdbcTemplate");
        BankAccountDAOImpl bankAccountDAOImpl = (BankAccountDAOImpl) webApplicationContext
                .getBean("bankAccountDAO");
        assertEquals("JDBC Template not Set", jdbcTemplate, bankAccountDAOImpl.getJdbcTemplate());

    }


}