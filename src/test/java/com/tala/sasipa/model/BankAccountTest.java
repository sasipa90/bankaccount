package com.tala.sasipa.model;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class BankAccountTest {

    @Test
    public void toStringTest() throws NoSuchFieldException, IllegalAccessException {
        final BankAccount bankAccount = new BankAccount();
        bankAccount.setAccountNumber("0001");
        bankAccount.setBalance(2000);
        bankAccount.setDailyDeposit(1000);
        bankAccount.setDailyWithdrawal(0);
        bankAccount.setDailyDepositAmount(1000);
        bankAccount.setDailyWithdrawalAmount(0);

        String expected = "Account Number : 0001 | Balance : 2000.0"
                + " | Daily Deposit : 1000"
                + " | Daily Withdrawal : 0"
                + " | Daily Deposit amount : 1000.0"
                + " | Daily Withdrawal Amount : 0.0";

        assertEquals("Values didn't match", expected, bankAccount.toString());
    }


    @Test
    public void accountNumberSetterTest() throws NoSuchFieldException, IllegalAccessException {
        final BankAccount bankAccount = new BankAccount();
        bankAccount.setAccountNumber("0001");

        final Field accountNumber = bankAccount.getClass().getDeclaredField("accountNumber");
        accountNumber.setAccessible(true);
        assertEquals("Fields didn't match", accountNumber.get(bankAccount), "0001");
    }

    @Test
    public void accountNumberGetterTest() throws NoSuchFieldException, IllegalAccessException {
        final BankAccount bankAccount = new BankAccount();
        final Field accountNumber = bankAccount.getClass().getDeclaredField("accountNumber");
        accountNumber.setAccessible(true);
        accountNumber.set(bankAccount, "0001");

        final String value = bankAccount.getAccountNumber();
        assertEquals("field wasn't retrieved properly", value, "0001");
    }


    @Test
    public void balanceGetterTest() throws NoSuchFieldException, IllegalAccessException {
        final BankAccount bankAccount = new BankAccount();
        final Field balance = bankAccount.getClass().getDeclaredField("balance");
        balance.setAccessible(true);
        balance.set(bankAccount, 1000.0);

        final double value = bankAccount.getBalance();
        assertEquals("field wasn't retrieved properly", value, 1000.0, 0.1);
    }

    @Test
    public void balanceSetterTest() throws NoSuchFieldException, IllegalAccessException {
        final BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(1000.0);

        final Field balance = bankAccount.getClass().getDeclaredField("balance");
        balance.setAccessible(true);
        assertEquals("Fields didn't match", balance.get(bankAccount), 1000.0);
    }

    @Test
    public void dailyDepositGetterTest() throws NoSuchFieldException, IllegalAccessException {
        final BankAccount bankAccount = new BankAccount();
        final Field dailyDeposit = bankAccount.getClass().getDeclaredField("dailyDeposit");
        dailyDeposit.setAccessible(true);
        dailyDeposit.set(bankAccount, 100);

        final int value = bankAccount.getDailyDeposit();
        assertEquals("field wasn't retrieved properly", value, 100);
    }

    @Test
    public void dailyDepositSetterTest() throws NoSuchFieldException, IllegalAccessException {
        final BankAccount bankAccount = new BankAccount();
        bankAccount.setDailyDeposit(100);

        final Field dailyDeposit = bankAccount.getClass().getDeclaredField("dailyDeposit");
        dailyDeposit.setAccessible(true);
        assertEquals("Fields didn't match", dailyDeposit.get(bankAccount), 100);
    }

     @Test
    public void dailyWithdrawalGetterTest() throws NoSuchFieldException, IllegalAccessException {
        final BankAccount bankAccount = new BankAccount();
        final Field dailyWithdrawal = bankAccount.getClass().getDeclaredField("dailyWithdrawal");
        dailyWithdrawal.setAccessible(true);
        dailyWithdrawal.set(bankAccount, 150);

        final int value = bankAccount.getDailyWithdrawal();
        assertEquals("field wasn't retrieved properly", value, 150);
    }

    @Test
    public void dailyWithdrawalSetterTest() throws NoSuchFieldException, IllegalAccessException {
        final BankAccount bankAccount = new BankAccount();
        bankAccount.setDailyWithdrawal(120);

        final Field dailyWithdrawal = bankAccount.getClass().getDeclaredField("dailyWithdrawal");
        dailyWithdrawal.setAccessible(true);
        assertEquals("Fields didn't match", dailyWithdrawal.get(bankAccount), 120);
    }

    @Test
    public void dailyDepositAmountGetterTest() throws NoSuchFieldException, IllegalAccessException {
        final BankAccount bankAccount = new BankAccount();
        final Field dailyDepositAmount = bankAccount.getClass().getDeclaredField("dailyDepositAmount");
        dailyDepositAmount.setAccessible(true);
        dailyDepositAmount.set(bankAccount, 1500.0);

        final double value = bankAccount.getDailyDepositAmount();
        assertEquals("field wasn't retrieved properly", value, 1500.0, 0.1);
    }

    @Test
    public void dailyDepositAmountSetterTest() throws NoSuchFieldException, IllegalAccessException {
        final BankAccount bankAccount = new BankAccount();
        bankAccount.setDailyDepositAmount(1200.0);

        final Field dailyDepositAmount = bankAccount.getClass().getDeclaredField("dailyDepositAmount");
        dailyDepositAmount.setAccessible(true);
        assertEquals("Fields didn't match", dailyDepositAmount.get(bankAccount), 1200.0);
    }

    @Test
    public void dailyWithdrawalAmountGetterTest() throws NoSuchFieldException, IllegalAccessException {
        final BankAccount bankAccount = new BankAccount();
        final Field dailyWithdrawalAmount = bankAccount.getClass().getDeclaredField("dailyWithdrawalAmount");
        dailyWithdrawalAmount.setAccessible(true);
        dailyWithdrawalAmount.set(bankAccount, 12000.0);

        final double value = bankAccount.getDailyWithdrawalAmount();
        assertEquals("field wasn't retrieved properly", value, 12000.0, 0.1);
    }

    @Test
    public void dailyWithdrawalAmountSetterTest() throws NoSuchFieldException, IllegalAccessException {
        final BankAccount bankAccount = new BankAccount();
        bankAccount.setDailyWithdrawalAmount(15000.0);

        final Field dailyWithdrawalAmount = bankAccount.getClass().getDeclaredField("dailyWithdrawalAmount");
        dailyWithdrawalAmount.setAccessible(true);
        assertEquals("Fields didn't match", dailyWithdrawalAmount.get(bankAccount), 15000.0);
    }

}
