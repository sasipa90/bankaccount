package com.tala.sasipa.service.impl;

import com.tala.sasipa.dao.BankAccountDAO;
import com.tala.sasipa.dao.impl.BankAccountDAOImpl;
import com.tala.sasipa.model.BankAccount;
import com.tala.sasipa.utils.JSONResponse;
import com.tala.sasipa.utils.Props;
import com.tala.sasipa.utils.Utilities;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.JsonExpectationsHelper;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:springrest-servlet.xml"})
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BankAccountServiceImplTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private static final String accountNo = UUID.randomUUID().toString();


    @Before
    public void setUp() throws Exception {
        BankAccountDAOImpl bankAccountDAOImpl = (BankAccountDAOImpl) webApplicationContext
                .getBean("bankAccountDAO");
        Props prop = (Props) webApplicationContext.getBean("props");
        bankAccountDAOImpl.setupAccount(accountNo, prop);

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    public void testDepositBusinessRules() throws Exception {

        Props prop = (Props) webApplicationContext.getBean("props");
        BankAccountServiceImpl bankAccountService = (BankAccountServiceImpl) webApplicationContext
                .getBean("bankAccountService");
        JSONResponse jsonResponse = bankAccountService
                .getAccountInformations(UUID.randomUUID().toString(), prop);

        JSONResponse response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgNotFound());
        assertEquals(response.toString(), jsonResponse.toString());

        jsonResponse = bankAccountService.deposit(UUID.randomUUID().toString(),
                prop.getSetupDepositAmount(), prop);

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgNotFound());
        assertEquals(response.toString(), jsonResponse.toString());

        jsonResponse = bankAccountService.deposit(accountNo, 0, prop);

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgDepositNegativeAmount());
        assertEquals(response.toString(), jsonResponse.toString());

        jsonResponse = bankAccountService.deposit(accountNo,
                (prop.getMaxDepositPerTrx() + prop.getSetupDepositAmount()),
                prop);

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgDepositExceededMaxPerTrx());
        assertEquals(response.toString(), jsonResponse.toString());

        jsonResponse = bankAccountService.deposit(accountNo,
                (prop.getMaxDepositPerDay() + prop.getSetupDepositAmount()),
                prop);

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgDepositExceededMaxPerDay());
        assertEquals(response.toString(), jsonResponse.toString());

        boolean continueRunning = true;
        while (continueRunning) {

            jsonResponse = bankAccountService.deposit(accountNo, 1, prop);
            if (jsonResponse.getMessage()
                    .equals(prop.getMsgDepositExceededFrequency())) {
                continueRunning = false;
            }
        }

        response = new JSONResponse();
        response.setStatusCode(HttpStatus.OK.value());
        response.setSuccess(false);
        response.setMessage(prop.getMsgDepositExceededFrequency());

    }

    @Test
    public void testWithdrawalBusinessRules() throws Exception {

        Props prop = (Props) webApplicationContext.getBean("props");
        BankAccountServiceImpl bankAccountService = (BankAccountServiceImpl) webApplicationContext
                .getBean("bankAccountService");

        JSONResponse jsonResponse = bankAccountService.withdraw(
                UUID.randomUUID().toString(), prop.getSetupDepositAmount(),
                prop);

        JSONResponse response = new JSONResponse();
        response.setStatusCode(HttpStatus.OK.value());
        response.setSuccess(false);
        response.setMessage(prop.getMsgNotFound());
        assertEquals(response.toString(), jsonResponse.toString());


        String accountNo = UUID.randomUUID().toString();
        BankAccountDAOImpl bankAccountDAOImpl = (BankAccountDAOImpl) webApplicationContext
                .getBean("bankAccountDAO");
        bankAccountDAOImpl.setupAccount(accountNo, prop);


        jsonResponse = bankAccountService.withdraw(accountNo, 0, prop);

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgWithdrawalNegativeAmount());
        assertEquals(response.toString(), jsonResponse.toString());

        jsonResponse = bankAccountService.withdraw(accountNo,
                (prop.getMaxWithdrawalPerTrx() + prop.getSetupWithdrawAmount()),
                prop);

        response = new JSONResponse();
        response.setStatusCode(HttpStatus.OK.value());
        response.setSuccess(false);
        response.setMessage(prop.getMsgWithdrawalExceeededMaxPerTrx());
        assertEquals(response.toString(), jsonResponse.toString());

        jsonResponse = bankAccountService.withdraw(accountNo,
                (prop.getMaxWithdrawalPerDay() + prop.getSetupWithdrawAmount()),
                prop);

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgWithdrawalExceededMaxPerDay());
        assertEquals(response.toString(), jsonResponse.toString());

        BankAccountDAO dao = bankAccountService.getBankAccountDAO();
        BankAccount account = dao.getAccountInformations(accountNo, prop);

        jsonResponse = bankAccountService.withdraw(accountNo,
                (account.getBalance() + 1), prop);

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgWithdrawalExceededBalance());
        assertEquals(response.toString(), jsonResponse.toString());

        boolean continueRunning = true;
        while (continueRunning) {

            jsonResponse = bankAccountService.withdraw(accountNo, 1, prop);
            if (jsonResponse.getMessage()
                    .equals(prop.getMsgWithdrawalExceededMaxFrequency())) {
                continueRunning = false;
            }
        }

        response = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(prop.getMsgWithdrawalExceededMaxFrequency());

    }


    @Test
    public void getAccountInformations() {
        Props prop = (Props) webApplicationContext.getBean("props");
        BankAccountServiceImpl bankAccountService = (BankAccountServiceImpl) webApplicationContext
                .getBean("bankAccountService");
        String accountNo = UUID.randomUUID().toString();
        BankAccountDAOImpl bankAccountDAOImpl = (BankAccountDAOImpl) webApplicationContext
                .getBean("bankAccountDAO");
        bankAccountDAOImpl.setupAccount(accountNo, prop);
        JSONResponse response = bankAccountService.getAccountInformations(accountNo, prop);
        JSONResponse expectedResponse = new JSONResponse();
        expectedResponse.setSuccess(true);
        expectedResponse.setMessage(Utilities.GET_BALANCE_MSG(700.0));
        assertEquals(response.toString(), expectedResponse.toString());
    }
}