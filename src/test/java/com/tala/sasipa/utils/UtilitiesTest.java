package com.tala.sasipa.utils;

import org.junit.Test;

import javax.rmi.CORBA.Util;

import static org.junit.Assert.*;

public class UtilitiesTest {

    @Test
    public void testClass() {
        Utilities utilities = new Utilities();
        assertNotNull("Object is null", utilities);
    }

    @Test
    public void GET_DEPOSIT_SUCCESS_MSG() {
        String expected = "Successfully deposited $1000.0"
            + " to account No 0001. Your new balance is $1000.0.";
        String actual = Utilities.GET_DEPOSIT_SUCCESS_MSG("0001", 1000, 1000);
        assertEquals("Value didn't match", expected, actual);
    }

    @Test
    public void GET_WITHDRAW_SUCCESS_MSG() {
        String expected = "Successfully withdrawn $1200.0 from account No 0001"
                + ". Your new balance is $0.0.";
        String actual = Utilities.GET_WITHDRAW_SUCCESS_MSG("0001", 1200, 0);
        assertEquals("Value didn't match", expected, actual);
    }

    @Test
    public void GET_BALANCE_MSG() {
        String expected = "Your balance is $1500.0.";
        String actual = Utilities.GET_BALANCE_MSG(1500);
        assertEquals("Value didn't match", expected, actual);
    }
}