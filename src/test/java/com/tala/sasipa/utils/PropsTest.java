package com.tala.sasipa.utils;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:springrest-servlet.xml"})
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PropsTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Test
    public void testProp(){
        Props prop = new Props();
        assertNotNull("Properties not null", prop);
    }

    @Test
    public void testToString() {

        Props prop = (Props) webApplicationContext.getBean("props");
        String expected = "defaultAccountNumber : ACC001 | deposit : deposit | withdraw : withdraw | " +
                "fetchLimit : 1 | dateFormat : YYYY-MM-DD | msgSuccess : Successfully fetched " +
                "data | msgError : An error occured while processing the request | " +
                "msgNotFound : The requested resource was not found | " +
                "msgDepositExceededMaxPerDay : Exceeded Maximum Deposit amount Per Day | " +
                "msgDepositExceededMaxPerTrx : Exceeded Maximum Deposit amount Per Transaction | " +
                "msgDepositExceededFrequency : Exceeded Maximum Deposit Frequency | " +
                "msgDepositNegativeAmount : Cannot deposit $0 or a negative amount | " +
                "msgWithdrawalExceededMaxPerDay : Exceeded Maximum Withdrawal Per Day | " +
                "msgWithdrawalExceeededMaxPerTrx: Exceeded Maximum Withdrawal Per Transaction | " +
                "msgWithdrawalExceededMaxFrequency : Exceeded Maximum Withdrawal Frequency | " +
                "msgWithdrawalExceededBalance : Cannot withdraw when balance is less than " +
                "withdrawal amount | msgWithdrawalNegativeAmount : Cannot withdraw $0 or a " +
                "negative amount | maxDepositPerDay : 150000.0 | maxDepositPerTrx : 40000.0 | " +
                "maxDepositDailyFrequency : 4 | maxWithdrawalPerDay : 50000.0 | " +
                "maxWithdrawalPerTrx : 20000.0 | maxWithdrawalDailyFrequency : 3";
        assertEquals("Property value is null", expected, prop.toString());
    }

    @Test
    public void getDefaultAccountNumber() {

        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getDefaultAccountNumber());
    }

    @Test
    public void getDeposit() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getDeposit());
    }

    @Test
    public void getWithdraw() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getWithdraw());
    }

    @Test
    public void getFetchLimit() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getFetchLimit());
    }

    @Test
    public void getDateFormat() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getDateFormat());
    }

    @Test
    public void getMsgSuccess() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMsgSuccess());
    }

    @Test
    public void getMsgError() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMsgError());
    }

    @Test
    public void getMsgNotFound() {

        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMsgNotFound());
    }

    @Test
    public void getMsgDepositExceededMaxPerDay() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMsgDepositExceededMaxPerDay());
    }

    @Test
    public void getMsgDepositExceededMaxPerTrx() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMsgDepositExceededMaxPerTrx());
    }

    @Test
    public void getMsgDepositExceededFrequency() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMsgDepositExceededFrequency());
    }

    @Test
    public void getMsgDepositNegativeAmount() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMsgDepositNegativeAmount());
    }

    @Test
    public void getMsgWithdrawalExceededMaxPerDay() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMsgWithdrawalExceededMaxPerDay());
    }

    @Test
    public void getMsgWithdrawalExceeededMaxPerTrx() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMsgWithdrawalExceeededMaxPerTrx());
    }

    @Test
    public void getMsgWithdrawalExceededMaxFrequency() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMsgWithdrawalExceededMaxFrequency());
    }

    @Test
    public void getMsgWithdrawalExceededBalance() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMsgWithdrawalExceededBalance());
    }

    @Test
    public void getMsgWithdrawalNegativeAmount() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMsgWithdrawalNegativeAmount());
    }

    @Test
    public void getMaxDepositPerDay() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMaxDepositPerDay());
    }

    @Test
    public void getMaxDepositPerTrx() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMaxDepositPerTrx());
    }

    @Test
    public void getMaxDepositDailyFrequency() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMaxDepositDailyFrequency());
    }

    @Test
    public void getMaxWithdrawalPerDay() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMaxWithdrawalPerDay());
    }

    @Test
    public void getMaxWithdrawalPerTrx() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMaxWithdrawalPerTrx());
    }

    @Test
    public void getMaxWithdrawalDailyFrequency() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getMaxWithdrawalDailyFrequency());
    }

    @Test
    public void getSetupDepositAmount() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getSetupDepositAmount());
    }

    @Test
    public void getSetupWithdrawAmount() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getSetupWithdrawAmount());
    }

    @Test
    public void getInputStream() {
        Props prop = (Props) webApplicationContext.getBean("props");
        assertNotNull("Properties is null", prop.getInputStream());
    }

    @Test
    public void setInputStream() {

        Props prop = (Props) webApplicationContext.getBean("props");
        prop.setInputStream(null);
        assertNull("Properties is null", prop.getInputStream());
    }
}