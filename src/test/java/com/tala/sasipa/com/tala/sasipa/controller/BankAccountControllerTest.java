package com.tala.sasipa.com.tala.sasipa.controller;

import com.tala.sasipa.dao.impl.BankAccountDAOImpl;
import com.tala.sasipa.utils.Props;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.hamcrest.Matchers.hasKey;


import java.util.Random;
import java.util.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:springrest-servlet.xml"})
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BankAccountControllerTest {

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;
    private static final String accountNo = UUID.randomUUID().toString();
    private final static Random r = new Random();
    private final static double rangeMin = 100;
    private final static double rangeMax = 1000;
    private final double depositAmount;
    private final double withrdrawAmount;

    public BankAccountControllerTest() {
        depositAmount = rangeMax + (rangeMax - rangeMin) * r.nextDouble();
        withrdrawAmount = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
        System.out.println(depositAmount + " - " + withrdrawAmount);
    }

    @Before
    public void setUp() {

        BankAccountDAOImpl bankAccountDAOImpl = (BankAccountDAOImpl) webApplicationContext
                .getBean("bankAccountDAO");
        Props prop = (Props) webApplicationContext.getBean("props");
        bankAccountDAOImpl.setupAccount(accountNo, prop);

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    public void balance() throws Exception {

        mockMvc.perform(get("/balance/" + accountNo)).andExpect(status().isOk())
                .andExpect(
                        content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasKey("success")))
                .andExpect(jsonPath("$", hasKey("statusCode")))
                .andExpect(jsonPath("$", hasKey("message")));

    }

    @Test
    public void deposit() throws Exception {

        mockMvc.perform(post("/deposit")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("amount", String.valueOf(depositAmount))
                .param("accountNo", accountNo))
                .andExpect(status().isOk())
                .andExpect(
                        content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasKey("success")))
                .andExpect(jsonPath("$", hasKey("statusCode")))
                .andExpect(jsonPath("$", hasKey("message")));

    }

    @Test
    public void withdraw() throws Exception {

        mockMvc.perform(post("/withdraw")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("amount", String.valueOf(withrdrawAmount))
                .param("accountNo", accountNo))
                .andExpect(status().isOk())
                .andExpect(
                        content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasKey("success")))
                .andExpect(jsonPath("$", hasKey("statusCode")))
                .andExpect(jsonPath("$", hasKey("message")));

    }



}
