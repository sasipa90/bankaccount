package com.tala.sasipa.dao;

import com.tala.sasipa.model.BankAccount;
import com.tala.sasipa.utils.Props;

public interface BankAccountDAO {

	BankAccount getAccountInformations(String accountNumber,
			Props props);

	BankAccount deposit(BankAccount account, double amount, Props props);

	BankAccount withdraw(BankAccount account, double amount,
			Props props);

	void setupAccount(String account, Props prop);
}
