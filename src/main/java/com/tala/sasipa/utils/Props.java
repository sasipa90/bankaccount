package com.tala.sasipa.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Application Properties.
 * 
 * Props.java
 * 
 * @author Parfait Pascal Sibomana
 */
public class Props {

	private static final Logger log = LogManager.getLogger(Properties.class);

	private InputStream inputStream;

	private String defaultAccountNumber;

	private String deposit;
	private String withdraw;

	private int fetchLimit;

	private String dateFormat;

	private String msgSuccess;
	private String msgError;
	private String msgNotFound;

	private String msgDepositExceededMaxPerDay;
	private String msgDepositExceededMaxPerTrx;
	private String msgDepositExceededFrequency;
	private String msgDepositNegativeAmount;

	private String msgWithdrawalExceededMaxPerDay;
	private String msgWithdrawalExceeededMaxPerTrx;
	private String msgWithdrawalExceededMaxFrequency;
	private String msgWithdrawalExceededBalance;
	private String msgWithdrawalNegativeAmount;

	private double maxDepositPerDay;
	private double maxDepositPerTrx;
	private int maxDepositDailyFrequency;

	private double maxWithdrawalPerDay;
	private double maxWithdrawalPerTrx;
	private int maxWithdrawalDailyFrequency;

	private double setupDepositAmount;
	private double setupWithdrawAmount;

	public Props() {

		log.info("Loading Application configurations.");

		try {
			Properties prop = new Properties();
			String propFileName = "bankaccount.properties";

			inputStream = getClass().getClassLoader()
					.getResourceAsStream(propFileName);

			if (inputStream != null) {
				prop.load(inputStream);

				defaultAccountNumber = prop.getProperty("DEFAULT_ACCOUNT_NO");
				deposit = prop.getProperty("DEPOSIT");
				withdraw = prop.getProperty("WITHDRAW");
				fetchLimit = Integer.valueOf(prop.getProperty("FETCH_LIMIT"));
				dateFormat = prop.getProperty("DATE_FORMAT");
				msgSuccess = prop.getProperty("MSG_SUCCESS");
				msgError = prop.getProperty("MSG_ERROR");
				msgNotFound = prop.getProperty("MSG_NOT_FOUND");

				msgDepositExceededMaxPerDay = prop
						.getProperty("MSG_DEPOSIT_EXCEEDED_MAX_PER_DAY");
				msgDepositExceededMaxPerTrx = prop
						.getProperty("MSG_DEPOSIT_EXCEEDED_MAX_PER_TRX");
				msgDepositExceededFrequency = prop
						.getProperty("MSG_DEPOSIT_EXCEEDED_MAX_FREQUENCY");
				msgDepositNegativeAmount = prop
						.getProperty("MSG_DEPOSIT_NEGATIVE_AMOUNT");

				msgWithdrawalExceededMaxPerDay = prop
						.getProperty("MSG_WITHDRAWAL_EXCEEDED_MAX_PER_DAY");
				msgWithdrawalExceeededMaxPerTrx = prop
						.getProperty("MSG_WITHDRAWAL_EXCEEDED_MAX_PER_TRX");
				msgWithdrawalExceededMaxFrequency = prop
						.getProperty("MSG_WITHDRAWAL_EXCEEDED_MAX_FREQUENCY");
				msgWithdrawalExceededBalance = prop
						.getProperty("MSG_WITHDRAWAL_EXCEEDED_MAX_AMOUNT");
				msgWithdrawalNegativeAmount = prop
						.getProperty("MSG_WITHDRAWAL_NEGATIVE_AMOUNT");

				maxDepositPerDay = Double
						.valueOf(prop.getProperty("MAX_DEPOSIT_PER_DAY"));
				maxDepositPerTrx = Double
						.valueOf(prop.getProperty("MAX_DEPOSIT_PER_TRX"));
				maxDepositDailyFrequency = Integer.valueOf(
						prop.getProperty("MAX_DEPOSIT_DAILY_FREQUENCY"));

				maxWithdrawalPerDay = Double
						.valueOf(prop.getProperty("MAX_WITHDRAWAL_PER_DAY"));
				maxWithdrawalPerTrx = Double
						.valueOf(prop.getProperty("MAX_WITHDRAWAL_PER_TRX"));
				maxWithdrawalDailyFrequency = Integer.valueOf(
						prop.getProperty("MAX_WITHDRAW_DAILY_FREQUENCY"));

				setupDepositAmount = Double
						.valueOf(prop.getProperty("SETUP_DEPOSIT_AMOUNT"));
				setupWithdrawAmount = Double
						.valueOf(prop.getProperty("SETUP_WITHDRAW_AMOUNT"));

				log.info("Successfully loaded properties : Props : "
						+ toString());

			} else {
				log.fatal("property file '" + propFileName + "' not found "
						+ "in the classpath. The application will stop");
				System.exit(-1);
			}

		} catch (Exception e) {
			log.error("Exception: " + e);
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				log.error("Exception: " + e);
			}
		}
	}

	@Override
	public String toString() {
		return "defaultAccountNumber : " + defaultAccountNumber
				+ " | deposit : " + deposit + " | withdraw : " + withdraw
				+ " | fetchLimit : " + fetchLimit + " | dateFormat : "
				+ dateFormat + " | msgSuccess : " + msgSuccess
				+ " | msgError : " + msgError + " | msgNotFound : "
				+ msgNotFound + " | msgDepositExceededMaxPerDay : "
				+ msgDepositExceededMaxPerDay
				+ " | msgDepositExceededMaxPerTrx : "
				+ msgDepositExceededMaxPerTrx
				+ " | msgDepositExceededFrequency : "
				+ msgDepositExceededFrequency + " | msgDepositNegativeAmount : "
				+ msgDepositNegativeAmount
				+ " | msgWithdrawalExceededMaxPerDay : "
				+ msgWithdrawalExceededMaxPerDay
				+ " | msgWithdrawalExceeededMaxPerTrx: "
				+ msgWithdrawalExceeededMaxPerTrx
				+ " | msgWithdrawalExceededMaxFrequency : "
				+ msgWithdrawalExceededMaxFrequency
				+ " | msgWithdrawalExceededBalance : "
				+ msgWithdrawalExceededBalance
				+ " | msgWithdrawalNegativeAmount : "
				+ msgWithdrawalNegativeAmount + " | maxDepositPerDay : "
				+ maxDepositPerDay + " | maxDepositPerTrx : " + maxDepositPerTrx
				+ " | maxDepositDailyFrequency : " + maxDepositDailyFrequency
				+ " | maxWithdrawalPerDay : " + maxWithdrawalPerDay
				+ " | maxWithdrawalPerTrx : " + maxWithdrawalPerTrx
				+ " | maxWithdrawalDailyFrequency : "
				+ maxWithdrawalDailyFrequency;

	}

	public String getDefaultAccountNumber() {
		return defaultAccountNumber;
	}

	public String getDeposit() {
		return deposit;
	}

	public String getWithdraw() {
		return withdraw;
	}

	public int getFetchLimit() {
		return fetchLimit;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public String getMsgSuccess() {
		return msgSuccess;
	}

	public String getMsgError() {
		return msgError;
	}

	public String getMsgNotFound() {
		return msgNotFound;
	}

	public String getMsgDepositExceededMaxPerDay() {
		return msgDepositExceededMaxPerDay;
	}

	public String getMsgDepositExceededMaxPerTrx() {
		return msgDepositExceededMaxPerTrx;
	}

	public String getMsgDepositExceededFrequency() {
		return msgDepositExceededFrequency;
	}

	public String getMsgDepositNegativeAmount() {
		return msgDepositNegativeAmount;
	}

	public String getMsgWithdrawalExceededMaxPerDay() {
		return msgWithdrawalExceededMaxPerDay;
	}

	public String getMsgWithdrawalExceeededMaxPerTrx() {
		return msgWithdrawalExceeededMaxPerTrx;
	}

	public String getMsgWithdrawalExceededMaxFrequency() {
		return msgWithdrawalExceededMaxFrequency;
	}

	public String getMsgWithdrawalExceededBalance() {
		return msgWithdrawalExceededBalance;
	}

	public String getMsgWithdrawalNegativeAmount() {
		return msgWithdrawalNegativeAmount;
	}

	public double getMaxDepositPerDay() {
		return maxDepositPerDay;
	}

	public double getMaxDepositPerTrx() {
		return maxDepositPerTrx;
	}

	public int getMaxDepositDailyFrequency() {
		return maxDepositDailyFrequency;
	}

	public double getMaxWithdrawalPerDay() {
		return maxWithdrawalPerDay;
	}

	public double getMaxWithdrawalPerTrx() {
		return maxWithdrawalPerTrx;
	}

	public int getMaxWithdrawalDailyFrequency() {
		return maxWithdrawalDailyFrequency;
	}

	public double getSetupDepositAmount() {
		return setupDepositAmount;
	}

	public double getSetupWithdrawAmount() {
		return setupWithdrawAmount;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

}
