package com.tala.sasipa.utils;

/**
 * Utilities Class.
 * 
 * Utilities.java
 * 
 * @author Parfait Pascal Sibomana
 */
public class Utilities {

	/**
	 * Generate successful deposit message
	 * @param account Account Number 
	 * @param amount amount deposited
	 * @param balance Updated balance
	 * @return String Message to be returned
	 */
	public static String GET_DEPOSIT_SUCCESS_MSG(String account, double amount,
			double balance) {
                return "Successfully deposited $" + amount 
                        + " to account No " + account 
                        + ". Your new balance is $" + balance + ".";
	}


	/**
	 * Generate successful withdrawal message
	 * @param account Account Number 
	 * @param amount amount deposited
	 * @param balance Updated balance
	 * @return String Message to be returned
	 */
	public static String GET_WITHDRAW_SUCCESS_MSG(String account, double amount,
			double balance) {
		return "Successfully withdrawn $" + amount 
                        + " from account No " + account 
                        + ". Your new balance is $" + balance + ".";
	}


	/**
	 * Generate get balance message
	 * @param balance balance
	 * @return String Message to be returned
	 */
	public static String GET_BALANCE_MSG(double balance) {
		return  "Your balance is $" + balance + ".";
	}

}
