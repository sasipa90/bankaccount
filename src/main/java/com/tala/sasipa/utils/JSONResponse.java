package com.tala.sasipa.utils;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Application JSON standard response class.
 * 
 * @author Parfait Pascal Sibomana <sasipa90@gmail.com>
 *
 */
@JsonInclude(Include.NON_NULL)
public class JSONResponse implements Serializable {
	private static final long serialVersionUID = -1936241990759033043L;

	private boolean success = true;
	private int statusCode = HttpStatus.OK.value();
	private String message;

	@Override
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return "";
		}
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
