package com.tala.sasipa.model;

import java.io.Serializable;

/**
 * Bank Account Model Class. BankAccount.java
 * 
 * @author Parfait Pascal Sibomana
 */
public class BankAccount implements Serializable{

	private String accountNumber;
	private double balance;
	private int dailyDeposit;
	private int dailyWithdrawal;
	private double dailyDepositAmount;
	private double dailyWithdrawalAmount;

	@Override
	public String toString() {
		return "Account Number : " + accountNumber + " | Balance : " + balance
				+ " | Daily Deposit : " + dailyDeposit
				+ " | Daily Withdrawal : " + dailyWithdrawal
				+ " | Daily Deposit amount : " + dailyDepositAmount
				+ " | Daily Withdrawal Amount : " + dailyWithdrawalAmount;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getDailyDeposit() {
		return dailyDeposit;
	}

	public void setDailyDeposit(int dailyDeposit) {
		this.dailyDeposit = dailyDeposit;
	}

	public int getDailyWithdrawal() {
		return dailyWithdrawal;
	}

	public void setDailyWithdrawal(int dailyWithdrawal) {
		this.dailyWithdrawal = dailyWithdrawal;
	}

	public double getDailyDepositAmount() {
		return dailyDepositAmount;
	}

	public void setDailyDepositAmount(double dailyDepositAmount) {
		this.dailyDepositAmount = dailyDepositAmount;
	}

	public double getDailyWithdrawalAmount() {
		return dailyWithdrawalAmount;
	}

	public void setDailyWithdrawalAmount(double dailyWithdrawalAmount) {
		this.dailyWithdrawalAmount = dailyWithdrawalAmount;
	}

}
