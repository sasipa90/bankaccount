/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tala.sasipa.exceptions;

import java.io.IOException;
import java.net.BindException;
import java.util.Arrays;

import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.dao.QueryTimeoutException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import com.tala.sasipa.utils.JSONResponse;

@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(IOException.class)
    public JSONResponse handleIOException(IOException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(ex.getLocalizedMessage());
        response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return response;
    }

    @ExceptionHandler(ConversionNotSupportedException.class)
    public JSONResponse handleConversionNotSupportedException(
            ConversionNotSupportedException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(ex.getLocalizedMessage());
        response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return response;
    }

    @ExceptionHandler(BindException.class)
    public JSONResponse handleBindException(BindException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(ex.getLocalizedMessage());
        response.setStatusCode(HttpStatus.BAD_REQUEST.value());
        return response;
    }

    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    public JSONResponse handleHttpMediaTypeNotAcceptableException(
            HttpMediaTypeNotAcceptableException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        String message = "Not acceptable media type. Acceptable media type : "
                + Arrays.toString(ex.getSupportedMediaTypes().toArray());
        response.setMessage(message);
        response.setStatusCode(HttpStatus.NOT_ACCEPTABLE.value());
        return response;
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public JSONResponse handleHttpMediaTypeNotSupportedException(
            HttpMediaTypeNotSupportedException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        String message = "Not supported media type. Supported media type : "
                + Arrays.toString(ex.getSupportedMediaTypes().toArray());
        response.setMessage(message);
        response.setStatusCode(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value());
        return response;
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public JSONResponse handleHttpMessageNotReadableException(
            HttpMessageNotReadableException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(ex.getLocalizedMessage());
        response.setStatusCode(HttpStatus.BAD_REQUEST.value());
        return response;
    }

    @ExceptionHandler(HttpMessageNotWritableException.class)
    public JSONResponse handleHttpMessageNotWritableException(
            HttpMessageNotWritableException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(ex.getLocalizedMessage());
        response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return response;
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public JSONResponse handleHttpRequestMethodNotSupportedException(
            HttpRequestMethodNotSupportedException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        response.setMessage("Method : " + ex.getMethod() + " is not supported");
        response.setStatusCode(HttpStatus.METHOD_NOT_ALLOWED.value());
        return response;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public JSONResponse handleMethodArgumentNotValidException(
            MethodArgumentNotValidException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(ex.getLocalizedMessage());
        response.setStatusCode(HttpStatus.BAD_REQUEST.value());
        return response;
    }

    @ExceptionHandler(MissingPathVariableException.class)
    public JSONResponse handleMissingPathVariableException(
            MissingPathVariableException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        response.setMessage("Variable : " + ex.getVariableName() 
                + " is missing");
        response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return response;
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public JSONResponse handleMissingServletRequestParameterException(
            MissingServletRequestParameterException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        response.setMessage("Parameter : " + ex.getParameterName() 
                + " - " + ex.getParameterType() + " is missing");
        response.setStatusCode(HttpStatus.BAD_REQUEST.value());
        return response;
    }

    @ExceptionHandler(MissingServletRequestPartException.class)
    public JSONResponse handleMissingServletRequestPartException(
            MissingServletRequestPartException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        response.setMessage("Request part : " + ex.getRequestPartName() 
                + " is missing");
        response.setStatusCode(HttpStatus.BAD_REQUEST.value());
        return response;
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public JSONResponse handleNoHandlerFoundException(
            NoHandlerFoundException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        response.setMessage("Not handler found for " + ex.getRequestURL() + 
                " using " + ex.getHttpMethod() + "mothod.");
        response.setStatusCode(HttpStatus.NOT_FOUND.value());
        return response;
    }

    @ExceptionHandler(NoSuchRequestHandlingMethodException.class)
    public JSONResponse handleNoSuchRequestHandlingMethodException(
            NoSuchRequestHandlingMethodException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        response.setMessage("Not handler found for " + ex.getMethodName() 
                + " mothod.");
        response.setStatusCode(HttpStatus.NOT_FOUND.value());
        return response;
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public JSONResponse handleMethodArgumentTypeMismatchException(
            MethodArgumentTypeMismatchException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        response.setMessage("Invalid given value '" + ex.getValue().toString() + "' "
        		+ "for '" + ex.getName() + "' argument.");
        response.setStatusCode(HttpStatus.BAD_REQUEST.value());
        return response;
    }

    @ExceptionHandler(QueryTimeoutException.class)
    public JSONResponse handlQueryTimeoutException(
    		QueryTimeoutException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(ex.getLocalizedMessage());
        response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return response;
    }
    
    @ExceptionHandler(ClassCastException.class)
    public JSONResponse handlClassCastException(
    		ClassCastException ex) {
        JSONResponse response  = new JSONResponse();
        response.setSuccess(false);
        response.setMessage(ex.getLocalizedMessage());
        response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return response;
    }
}